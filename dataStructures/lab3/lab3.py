# lab3.py


# This is an example of a "driver" script. It imports the
# TemperatureSensor module, makes an object, and calls a few methods.

import sys
from TemperatureSensor import TemperatureSensor

if __name__=='__main__':
    try:
        mySensor = TemperatureSensor(inputfile=sys.argv[1])
        print(mySensor)
        mySensor.load_data()
        print(mySensor)
        mySensor.convert_temperatures()
        print(mySensor)
        try:
            mySensor.save_data(sys.argv[2])
        except:
            mySensor.save_data()
    except:
        print('An exceptional condition occurred:')
        #print(e)
        # "<argument>" is a conventional way of saying an argument is optional.
        # <outputfile> means "Type the name of some real output file"
        # The "<" and ">" aren't actually typed by the user.
        # E.g., this could be run as:
        # $ python3 lab3.py fahrenheit_small.txt
        # or
        # $ python3 lab3.py fahrenheit_small.txt myoutput.txt
        print('Try running as: python3 lab3.py inputfile <outputfile>')
