import unittest


class Temps:
    def __init__(self):
        pass

    def celsius2fahrenheit(self, arrayOfTemps, reverse=False):
        # Converts an array of celsius temps to an array or fahrenheit temps or viceversa
        if reverse:
            return [(temp - 32) / 1.8 for temp in arrayOfTemps]
        return [temp * 1.8 + 32 for temp in arrayOfTemps]


class MyTest(unittest.TestCase):
    def testTempsClass(self):
        aE = self.assertEqual
        T = Temps()
        aE(T.celsius2fahrenheit([0, 20, 30, 100]), [32, 68, 86, 212])  # OK
        aE(T.celsius2fahrenheit([32, 68, 86, 212], reverse=True), [0, 20, 30, 100])  # OK


if __name__ == '__main__':
    unittest.main()
