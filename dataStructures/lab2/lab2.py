# lab2.py

from collections import deque


class Temps:
    def __init__(self):
        pass

    def fahrenheit2celsius(self, temps):
        '''Takes a Python list of Fahrenheit temperatures,
        returns a Python list of Celsius temperatures.
        '''
        return [(float(temp) - 32) / 1.8 for temp in temps]

    def celsius2fahrenheit(self, temps):
        '''Takes a Python list of Celsius temperatures,
        returns a Python list of Fahrenheit temperatures.
        '''
        return [float(temp) * 1.8 + 32 for temp in temps]

    def read_temp_file(self, filename):
        with open(filename, 'r') as f:
            data = deque(f.read().splitlines())

        tempType = data.popleft()
        return tempType, data

    def write_temp_file(self, temps, degree_type='Celsius', filename='out.txt'):
        '''Opens and writes a file of temperatures from a list. By default,
        it assumes Celsius. It writes the type of degree on the first line,
        and then all of the temperatures in the input list, one temperature
        per line.'''

        with open(filename, 'w') as f:
            f.write('{0}\n'.format(degree_type))
            for temp in temps:
                f.write('{0:.2f}\n'.format(temp))

    def get_stats(self, temps):
        '''Given a list of temperatures, returns a dictionary of the
        minimum, maximum, mean, and median temperatures.'''
        statsDict = {'min': None, 'max': None, 'mean': None, 'median': None}

        # May be helpful to implement this as a helper function, but not necessary
        def median(lst):
            '''returns the median of a list'''
            sortedArray = sorted(lst)
            middle, odd = divmod(len(sortedArray), 2)
            if odd:
                median = sortedArray[middle]
            else:
                median = (sortedArray[middle - 1] + sortedArray[middle]) / 2

            return median

        def mean(lst):
            # Returns the mean of a list
            return sum(lst) / len(lst)

        statsDict['min'] = min(temps)
        statsDict['max'] = max(temps)
        statsDict['median'] = median(temps)
        statsDict['mean'] = mean(temps)

        return statsDict

    def wrapper(self, filename):
        # Lab instructions in here
        #
        # Read from the file
        tempType, data = self.read_temp_file(filename)

        # Convert temps to other temp type
        if tempType == 'Fahrenheit':
            reverseTemps = self.fahrenheit2celsius(data)
            oppositeTempType = 'Celsius'
        elif tempType == 'Celsius':
            reverseTemps = self.celsius2fahrenheit(data)
            oppositeTempType = 'Fahrenheit'

        # Get the stats for the new temps
        stats = self.get_stats(reverseTemps)

        # Print the stats to the console
        print('\nconverted_{0}: {1}'.format(filename, stats), end='')

        # Write the new temps to a new file
        self.write_temp_file(temps=reverseTemps,
                             degree_type=oppositeTempType,
                             filename='out_{0}'.format(filename.title(
                             )))


if __name__ == '__main__':
    # Repeat the instructions for each file
    T = Temps()
    T.wrapper('fahrenheit_large.txt')
    T.wrapper('celsius_small.txt')
    T.wrapper('fahrenheit_small.txt')
    print('\n')
