# -*- coding: utf-8 -*-

# from __future__ import absolute_import, division
from flask import Flask, flash, redirect, render_template, request, url_for, session, Markup
from flask_sslify import SSLify
from flask_sqlalchemy import SQLAlchemy
# from wtforms import Form, BooleanField, TextField, PasswordField, validators
from MySQLdb import escape_string as thwart
from functools import wraps
# from encrypt import mainEncrypt, mainDecrypt
from main import Crypto
import time
import os
import bcrypt
import gc
# import shelve
import re
import secret

app = Flask(__name__)
currentDir = os.getcwd()
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{0}/db/default.db'.format(
    currentDir)
db = SQLAlchemy(app)
sslify = SSLify(app, subdomains=True, permanent=True)
try:
    app.secret_key = os.environ['SECRET_KEY']
except Exception:
    app.secret_key = secret.dataDatabasePassword()


# If a logged-out user navigates to a page only availble to users who have logged in
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('home'))

    return wrap


# If a logged-in user navigates to a page only availble to users who haven't logged in
def login_not_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' not in session:
            return f(*args, **kwargs)
        else:
            flash("You need to logout first")
            return redirect(url_for('main'))

    return wrap


@app.route('/home/')
def home():
    return render_template("home.html")


@app.route('/main/', methods=['GET', 'POST'])
@app.route('/', methods=['GET', 'POST'])
@login_required
def main():
    if request.method == "POST":
        user = thwart(session['username'])
        if request.form.get('decryptReturn') is not None:
            string = thwart(request.form['success'])
        else:
            string = thwart(request.form['string'])
        keyNum = thwart(request.form['number'])

        try:
            keyNum = int(str(keyNum, 'utf-8'))
        except ValueError:
            keyNum = 0

        if request.form.get('encrypt') is not None:
            try:
                returnValue = Crypto(str(string, 'utf-8')).caesar(keyNum)
                return render_template('main.html', success=returnValue)
            except Exception as e:
                return render_template('main.html', error=e)
        else:  # decrypt
            try:
                returnList = Crypto(str(string, 'utf-8')).decryptCaesar()
                displayString = '\n\n'.join(returnList)
                return render_template('main.html', success=displayString)
            except Exception as e:
                return render_template('main.html', error=e)

    gc.collect()
    return render_template("main.html")


@app.route("/logout/")
@login_required
def logout():
    session.clear()
    flash("You have been logged out")

    gc.collect()
    return render_template("home.html")


@app.route('/login/', methods=['GET', 'POST'])
@login_not_required
def login():
    error = ''
    if request.method == "POST":
        try:
            login = thwart(request.form['username'])
            login = str(login, 'utf-8')
            if '@' in login:
                userInfo = User.query.filter_by(email=login).first()
                username = userInfo.username
                password = userInfo.password
                timezone = userInfo.timezone
            else:
                userInfo = User.query.filter_by(username=login).first()
                username = userInfo.username
                password = userInfo.password
                timezone = userInfo.timezone
        except Exception as e:
            error = "Invalid credentials."
            gc.collect()
            print(e)
            return render_template("login.html", error=error)

        try:
            if bcrypt.checkpw(
                    bytes(request.form['password'], 'utf-8'),
                    bytes(password, 'utf-8')):
                session['logged_in'] = True
                session['username'] = username
                session['timezone'] = timezone
                session.modified = False

                flash("You are now logged in")

                gc.collect()
                return redirect(url_for("main"))
            else:
                error = "Invalid credentials"

        except Exception as e:
            error = "Invalid Credentials."
            print(e)
            gc.collect()
            return render_template("login.html", error=error)

    gc.collect()
    return render_template("login.html", error=error)


@app.route("/profile/")
@login_required
def profile():
    return render_template("profile.html")


@app.route("/update/", methods=['GET', 'POST'])
@login_required
def update():
    error = ''
    error1 = ''
    try:
        if request.method == "POST":
            if len(thwart(request.form['email'])) > 0:
                email = thwart(request.form['email'])
            else:
                email = None

            if thwart(request.form['timezone']) == '- None -' or thwart(
                    request.form['timezone']) == '':
                timezone = None
            else:
                timezone = thwart(request.form['timezone'])

            if len(thwart(request.form['oldpassword'])) > 0:
                oldpassword = thwart(request.form['oldpassword'])
            else:
                oldpassword = None

            if len(thwart(request.form['phone'])) > 0:
                phone = thwart(request.form['phone'])
            else:
                phone = None

            if (email) is not None:
                pass
            elif (phone) is not None:
                pass
            elif (timezone) is not None:
                timezone = thwart(request.form['timezone'])
                username = session['username']

                x = "UPDATE users SET timezone = ('%s') WHERE username = ('%s')" % (
                    thwart(timezone), thwart(username))

                flash("Timezone successfully changed to %s" %
                      (thwart(timezone)))
            elif (oldpassword) is not None:
                newpassword = thwart(request.form['newpassword'])
                newpasswordconfirm = thwart(request.form['newpasswordconfirm'])
                oldpassword = thwart(request.form['oldpassword'])

                if len(thwart(newpassword)) >= 6 and len(
                        thwart(newpassword)) <= 72:
                    password = thwart(request.form['password'])
                    relpassword = "^[^\0\s\b\f\n\r\v\t].[^\0\s\b\f\n\r\v\t]+$"
                    if re.match(relpassword, password) is None:
                        error1 = "New password is not okay"
                    else:
                        if thwart(newpasswordconfirm) == thwart(newpassword):
                            username = session['username']
                            x = "SELECT * FROM users WHERE username = ('%s')" % (
                                thwart(username))
                            c.execute(x)
                            password = c.fetchone()[2]

                            if bcrypt.hashpw(thwart(oldpassword), password):
                                newpassword = bcrypt.hashpw(
                                    (str(thwart(newpassword))),
                                    bcrypt.gensalt(17))

                                x = "UPDATE users SET password = ('%s') WHERE username = ('%s')" % (
                                    thwart(newpassword), thwart(username))

                                flash("Password successfully changed")

                            else:
                                error1 = "Password is not okay"

                        else:
                            error1 = "New passwords do not match"
                else:
                    error1 = "New password is not okay"

        gc.collect()
        return render_template("update.html", error1=error1)
    except Exception as e:
        error = "error"
        try:
            pass
        except Exception:
            pass
        return render_template("update.html", error=error)
    return render_template("update.html")


@app.route('/register/', methods=['GET', 'POST'])
@login_not_required
def register():
    try:
        # form = RegistrationForm(request.form)
        if request.method == "POST":
            username = thwart(request.form['username'])
            if len(username) >= 4:
                if len(username) <= 20:
                    relusername = b"^\w+$"
                    error1 = ''
                    if re.match(relusername, username) is None:
                        error1 = "Username is not okay"
                else:
                    error1 = "Username must be less than or equal to 20 characters"
            else:
                error1 = "Username must be greater than or equal to 4 characters"

            password = thwart(request.form['password'])
            if len(password) >= 6:
                if len(password) <= 144:
                    error3 = ''
                    relpassword = b"^[^\0\s\b\f\n\r\v\t].[^\0\s\b\f\n\r\v\t]+$"
                    if re.match(relpassword, password) is None:
                        error3 = "Password is not okay"
                else:
                    error3 = "Password must be less than or equal to 144 characters"
            else:
                error3 = "Password must be greater than or equal to 6 characters"

            phone = (thwart(request.form['phone']))
            if len(phone) >= 5:
                if len(phone) <= 20:
                    phone = phone.replace('(', '').replace(')', '').replace(
                        '-', '').replace(' ', '')
                    error5 = ''
                    relphone = b"^\d+$"
                    if re.match(relphone, phone) is None:
                        error5 = "Phone number is not okay"
                else:
                    error5 = "Phone number must be less than or equal to 20 characters"
            else:
                error5 = ''

            email = thwart(request.form['email'])
            if len(email) >= 6:
                if len(email) <= 50:
                    error2 = ''
                    relemail = b"^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$"
                    if re.match(relemail, email) is None:
                        error2 = "Email is not okay"
                else:
                    error2 = "Email must be less than or equal to 50 characters"
            else:
                error2 = ''

            if password != thwart(request.form['password_confirm']):
                error3 = "Passwords do not match"

            timezone = thwart(request.form['timezone'])

            if timezone == '- None -' or timezone == '':
                error7 = "Invalid Timezone"
            else:
                error7 = ''

            created = time.time()

            if error1 != '' or error2 != '' or error3 != '' or error5 != '' or error7 != '':
                print(error1)
                print(error2)
                print(error3)
                print(error5)
                print(error7)
                return render_template(
                    'register.html',
                    error1=error1,
                    error2=error2,
                    error3=error3,
                    error5=error5,
                    error7=error7)

            encryptedPassword = bcrypt.hashpw(password, bcrypt.gensalt(17))

            existingUsername = User.query.filter_by(username=username).first()
            if email != b'':
                existingEmail = User.query.filter_by(email=email).first()
                email = str(email, 'utf-8')
            else:
                email = None
                existingEmail = None
            if phone != b'':
                existingPhone = User.query.filter_by(phone=phone).first()
                str(phone, 'utf-8')
            else:
                phone = None
                existingPhone = None

            if existingUsername is not None or existingEmail is not None or existingPhone is not None:
                # flash not working currently
                flash("That username or email or phone is already taken")
                return ('', 204)

            newUser = User(
                str(username, 'utf-8'),
                str(encryptedPassword, 'utf-8'), email, phone,
                str(timezone, 'utf-8'), created)

            db.session.add(newUser)
            db.session.commit()

            flash("Welcome {0}!".format(str(username, 'utf-8')))
            gc.collect()

            session['logged_in'] = True
            session['username'] = thwart(request.form['username'])
            session['timezone'] = timezone
            session.modified = False

            flash("You are now logged in")

            gc.collect()
            return redirect(url_for("main"))

        else:
            return render_template("register.html")
    except Exception as e:
        print(e)
        return render_template("register.html", error=e)


@app.route("/privacy/")
def privacy():
    return render_template("privacy.html")


@app.route("/tos/")
def tos():
    return render_template("tos.html")


@app.route('/feedback/')
def feedback():
    return redirect(
        "https://docs.google.com/forms/d/e/1FAIpQLScWk0iVPjdlhwO3dJEmGB5_yqdGsr28dj-YKXrn00kgrOtrEg/viewform"
    )


@app.route('/help/')
def help():
    shh = Markup('<h1 style="color:red">shh</h1>')
    return render_template("help.html", shh=shh)


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html")


class User(db.Model):
    username = db.Column(db.String(32), primary_key=True)
    password = db.Column(db.String(128), unique=True)
    email = db.Column(db.String(64), unique=True)
    phone = db.Column(db.String(32), unique=True)
    timezone = db.Column(db.String(120), unique=False)
    created = db.Column(db.String(32), unique=True)

    def __init__(self, username, password, email, phone, timezone, created):
        self.username = username
        self.password = password
        self.email = email
        self.phone = phone
        self.timezone = timezone
        self.created = created

    def __repr__(self):
        return '<username {0}>, <password {1}>, <email {2}>, <phone {3}>  <timezone {4}>, <created {5}>'.format(
            self.username, self.password, self.email, self.phone,
            self.timezone, self.created)


#class RegistrationForm(Form):
#    pass
# username = TextField('Username', [validators.Length(min=4, max=20)])
# email = TextField('Email', [validators.Length(min=6, max=50)])
# password = PasswordField('Password', [validators.Required(), validators.EqualTo('confirm', message="Passwords must match"), validators.Length(min=6, max=72)])
# confirm = PasswordField('Repeat Password')

# accept_tos = BooleanField('I accept the <a href="/tos/">Terms of Service</a> and the <a href="/privacy/">Privacy Notice</a> (Last updated September 25, 2016)', [validators.Required()])

if __name__ == '__main__':
    app.run(host='localhost', debug=True, port=9874)
    # pass

# <script src="{{ url_for('static', filename='node_modules/toastr/toastr.js')}}"></script>
# https://codeburst.io/creating-a-full-stack-web-application-with-python-npm-webpack-and-react-8925800503d9
