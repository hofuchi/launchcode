#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest


class Counting:
    def __init__(self):
        pass

    def countChars(self, string):
        returnDict = {}
        splitString = set(list(string))
        for char in splitString:
            returnDict[char] = string.count(char)

        return returnDict

    def factorDict(self, num):
        resultDict = {}
        for num in range(2, num + 1):
            resultDict[num] = self.factors(num)

        return resultDict

    def factors(self, num):
        return [n for n in range(1, num + 1)if num % n == 0]

    def reverseDict(self, dicty):
        returnDict = {}
        for key, val in dicty.items():
            returnKey = [key]
            for num in range(len(val)):
                returnDict[val[num]] = returnKey

        return returnDict


class MyTest(unittest.TestCase):
    def test_me(self):
        C = Counting()
        self.assertEqual(C.countChars("abc123ABC123"), {
                         'a': 1, 'A': 1, 'c': 1, 'b': 1, 'C': 1, '1': 2, '3': 2, '2': 2, 'B': 1})  # OK
        self.assertEqual(C.factors(15), [1, 3, 5, 15])  # OK
        self.assertEqual(C.factorDict(6), {2: [1, 2], 3: [1, 3], 4: [1, 2, 4], 5: [1, 5], 6: [1, 2, 3, 6]})  # OK
        # Check if I'm doing this the expected way
        self.assertEqual(C.reverseDict({2: [1, 2, 2], 3: [3, 5]}), {1: [2], 2: [2], 3: [
                         3], 5: [3]})  # OK


if __name__ == '__main__':
    unittest.main()
